//
//  ViewController.m
//  sign up form
//
//  Created by Click Labs135 on 9/17/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


UIImageView *image;

UILabel *label1;

UITextField *textfield1;

UILabel *label2;

UITextField *textfield2;

UILabel *label3;

UITextField *textfield3;

UILabel *label4;

UITextField *textfield4;

UILabel *label5;

UITextField *textfield5;

UILabel *label6;

UITextField *textfield6;

UILabel *label7;

UIButton *button;

UIAlertView *alert1;

UIAlertView *alert2;

UIAlertView *alert3;

UIAlertView *alert4;

UIAlertView *alert5;

UIAlertView *alert6;






- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //set label for sign up
    
    UILabel *label7=[[UILabel alloc]init];
    
    label7.frame=CGRectMake(140,50,150,50);
    
    label7.text=@"SIGN UP";
    
    label7.textColor=[UIColor blueColor];
    
    [label7 setFont:[UIFont boldSystemFontOfSize:30]];
    
    [self.view addSubview:label7];
    
    
    
    
    
    
    
    
    
    //set a background image
    
    UIImageView *image=[[UIImageView alloc]init];
    
    image.frame=CGRectMake(0,0,380,700);
    
    
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"vv.jpg"]];
    
    [self.view addSubview:image];
    
    
    
    //set label for first name
    
    UILabel *label1=[[UILabel alloc]init];
    
    label1.frame=CGRectMake(10,100,200,50);
    
    label1.text=@"First Name:-";
    
    label1.textColor=[UIColor blackColor];
    
    [label1 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:label1];
    
    
    
    //set text for first name
    
    
    
    UITextField *textfield1=[[UITextField alloc]init];
    
    textfield1.frame=CGRectMake(130,105,230,40);
    
    textfield1.borderStyle=UITextBorderStyleRoundedRect;
    
    textfield1.placeholder=@"enter your first name";
    
    [textfield1 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:textfield1];
    
    
    
    
    
    //set label2 for last name
    
    UILabel *label2=[[UILabel alloc]init];
    
    label2.frame=CGRectMake(10,150,200,50);
    
    label2.text=@"Last Name:-";
    
    label2.textColor=[UIColor blackColor];
    
    [label2 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:label2];
    
    
    
    //set text2 for last name
    
    
    
    UITextField *textfield2=[[UITextField alloc]init];
    
    textfield2.frame=CGRectMake(130,155,230,40);
    
    textfield2.borderStyle=UITextBorderStyleRoundedRect;
    
    textfield2.placeholder=@"enter your last name";
    
    [textfield2 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:textfield2];
    
    
    
    
    
    //set label3 for email address
    
    UILabel *label3=[[UILabel alloc]init];
    
    label3.frame=CGRectMake(10,200,200,50);
    
    label3.text=@" Email:-";
    
    label3.textColor=[UIColor blackColor];
    
    [label3 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:label3];
    
    
    
    //set text3 for email address
    
    
    
    UITextField *textfield3=[[UITextField alloc]init];
    
    textfield3.frame=CGRectMake(130,205,230,40);
    
    textfield3.borderStyle=UITextBorderStyleRoundedRect;
    
    textfield3.placeholder=@"enter your Email";
    
    [textfield3 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:textfield3];
    
    
    
    
    
    //set label4 for passward
    
    UILabel *label4=[[UILabel alloc]init];
    
    label4.frame=CGRectMake(10,250,200,50);
    
    label4.text=@" Passward:-";
    
    label4.textColor=[UIColor blackColor];
    
    [label4 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:label4];
    
    
    
    
    
    //set text4 for passward
    
    UITextField *textfield4=[[UITextField alloc]init];
    
    textfield4.frame=CGRectMake(130,255,230,40);
    
    textfield4.borderStyle=UITextBorderStyleRoundedRect;
    
    textfield4.placeholder=@"enter your passward";
    
    [textfield4 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:textfield4];
    
    
    
    //set label5 for email address
    
    UILabel *label5=[[UILabel alloc]init];
    
    label5.frame=CGRectMake(0,300,230,50);
    
    label5.text=@" confirm passward:-";
    
    label5.textColor=[UIColor blackColor];
    
    [label5 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:label5];
    
    
    
    //set text5 for email address
    
    
    
    UITextField *textfield5=[[UITextField alloc]init];
    
    textfield5.frame=CGRectMake(130,305,230,40);
    
    textfield5.borderStyle=UITextBorderStyleRoundedRect;
    
    textfield5.placeholder=@"enter your confirm passward";
    
    [textfield5 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:textfield5];
    
    
    
    
    
    //set label6 for passward
    
    UILabel *label6=[[UILabel alloc]init];
    
    label6.frame=CGRectMake(10,350,200,50);
    
    label6.text=@" phone number:-";
    
    label6.textColor=[UIColor blackColor];
    
    [label6 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:label6];
    
    
    
    
    
    //set text6 for passward
    
    UITextField *textfield6=[[UITextField alloc]init];
    
    textfield6.frame=CGRectMake(130,355,230,40);
    
    textfield6.borderStyle=UITextBorderStyleRoundedRect;
    
    textfield6.placeholder=@"phone number";
    
    [textfield6 setFont:[UIFont boldSystemFontOfSize:15]];
    
    [self.view addSubview:textfield6];
    
    
    
    //set button for submit button
    
    UIButton *button=[[UIButton alloc]init];
    
    button.frame=CGRectMake(140,450,150,50);
    
    button.backgroundColor=[UIColor grayColor];
    
    [button setTitle:@"SUBMIT" forState:UIControlStateNormal];
    
    [button addTarget:self action:@selector(buttonpressed:) forControlEvents:UIControlEventTouchUpInside];
    
    button.titleLabel.font=[UIFont systemFontOfSize:18];
    [self.view addSubview:button];
    
    
    
    
    
    

    
}

//Text field 1
-(void)buttonpressed:(UIButton *)sender
{
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Sign up form"
                                                     message:@"your form has been confirm"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
    [alert1 show];

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
